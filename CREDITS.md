Data sources
Plates: https://github.com/fraxen/tectonicplates
Earthquakes: USGS earthquake feed https://earthquake.usgs.gov/earthquakes/feed/
Volcano: Smithsonian


Icons
Strato: Onlinewebfonts.com http://cdn.onlinewebfonts.com/svg/img_548299.png