var config = {
    "google_api_key": "",
    "mapbox_api_key": "",
    "styles": {
        "subduction" : "#11ff11",
        "cont_convergent": "#11ff11",
        "cont_rift": "#0000ff",
        "cont_transform": "#ff0000",
        "ocean_convergent": "#11ff11",
        "ocean_spread": "#1111ff",
        "ocean_transform": "#ff1111"
    }
}
